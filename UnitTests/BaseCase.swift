//
//  BaseCase.swift
//  UnitTests
//
//  Created by Oleksii Horishnii on 2/28/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

@testable import RxTv
import XCTest
import RxSwift

class BaseCase: XCTestCase {
    var resolver: Resolver!
    var disposeBag: DisposeBag!
    
    override func setUp() {
        super.setUp()
        
        self.resolver = Resolver()
        self.disposeBag = DisposeBag()
    }
    
    override func tearDown() {
        self.resolver = nil
        self.disposeBag = nil
        
        super.tearDown()
    }
}
