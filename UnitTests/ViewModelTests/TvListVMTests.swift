//
//  UnitTests.swift
//  UnitTests
//
//  Created by Oleksii Horishnii on 2/28/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

@testable import RxTv
import XCTest
import RxSwift
import RxTest
import RxCocoa

class TvListVMTests: BaseCase {
    var repo: MockedTvShowRepository!
    
    override func setUp() {
        super.setUp()
        
        self.repo = MockedTvShowRepository()
        self.resolver.tvShowRepository = { self.repo }
    }
    
    let testInputs = [
        TvShow(name: "The Hunger", posterPath: "p1"),
        TvShow(name: "What We Do in the Shadows", posterPath: "p2"),
        TvShow(name: "The Addams Family", posterPath: "p3"),
        TvShow(name: "Beetlejuice", posterPath: "p4"),
    ]
    lazy var testOutputs = self.testInputs
        .map { TvListCellVM.from(tvShow: $0) }
    lazy var sortedTestOutputs = self.testInputs
        .sorted { $0.name < $1.name }
        .map { TvListCellVM.from(tvShow: $0) }

    func testShows() {
        let vm: TvListVM = self.resolver.tvListVM()
        
        let scheduler = TestScheduler(initialClock: 0)
        
        // GIVEN
        let networkResponse = scheduler.createHotObservable([
            .next(210, self.testInputs),
            .completed(211)
        ])
        self.repo.topRatedOutput = networkResponse.asObservable()

        let sortButtonClick = scheduler.createHotObservable([
            .next(230, Void()),
            .next(250, Void()),
        ])
        let input = TvListVM.Input(
            sortButtonClicked: ControlEvent(events: sortButtonClick)
        )
        let output = vm.transform(input: input)

        // WHEN
        let showsObserver = scheduler.start {
            output.shows.asObservable()
        }
                
        // THEN
        let correctShows = Recorded.events(
            .next(210, self.testOutputs),
            .next(230, self.sortedTestOutputs),
            .next(250, self.sortedTestOutputs)
        )
        
        let networkSubscriptions = [
            // scheduler starts at 200
            // network observable completes at 211
            Subscription(200, 211)
        ]
        
        let sortButtonSubscriptions = [
            // scheduler starts at 200
            // scheduler disposed at 1000
            Subscription(200, 1000)
        ]
        
        XCTAssertEqual(showsObserver.events, correctShows)
        XCTAssertEqual(networkResponse.subscriptions, networkSubscriptions)
        XCTAssertEqual(sortButtonClick.subscriptions, sortButtonSubscriptions)
    }
}
