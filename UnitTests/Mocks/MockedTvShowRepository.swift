//
//  MockedTvShowRepository.swift
//  UnitTests
//
//  Created by Oleksii Horishnii on 2/28/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

@testable import RxTv
import XCTest
import RxSwift

class MockedTvShowRepository: TvShowRepository {
    var topRatedOutput: Observable<[TvShow]>!
    func topRated() -> Observable<[TvShow]> {
        return self.topRatedOutput
    }
}
