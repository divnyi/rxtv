//
//  Resolver.swift
//  RxTv
//
//  Created by Oleksii Horishnii on 2/8/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import UIKit

class Resolver {
    // MARK: welcome
    struct WelcomeInput {
        var tvListCoordinator: Coordinator!
    }
    var welcomeInput = WelcomeInput()

    lazy var welcomeScreenVC = { [unowned self] () -> UIViewController in
        return WelcomeVC.createVC(vm: self.welcomeScreenVM())
    }
    lazy var welcomeScreenVM = { [unowned self] () -> WelcomeVM in
        return WelcomeVMImpl(tvListCoordinator: self.welcomeInput.tvListCoordinator)
    }
    
    // MARK: tv shows
    lazy var tvListVC = { [unowned self] () -> UIViewController in
        return TvListVC.createVC(vm: self.tvListVM())
    }
    lazy var tvListVM = { () -> TvListVM in
        return TvListVMImpl(repo: self.tvShowRepository())
    }
    lazy var tvShowRepository = { () -> TvShowRepository in
        return TvShowRepositoryImpl()
    }
}
