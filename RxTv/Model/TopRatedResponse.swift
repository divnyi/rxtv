//
//  TopRatedResponse.swift
//  RxTv
//
//  Created by Oleksii Horishnii on 2/8/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import UIKit

struct TopRatedResponse: Decodable {
    var results: [TvShow]
}
