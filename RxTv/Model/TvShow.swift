//
//  TvShow.swift
//  RxTv
//
//  Created by Oleksii Horishnii on 2/8/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import Foundation

struct TvShow: Decodable {
    var name: String
    var posterPath: String
    
    enum CodingKeys: String, CodingKey {
        case name
        case posterPath = "poster_path"
    }
}
