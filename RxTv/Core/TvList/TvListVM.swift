//
//  TvListVM.swift
//  RxTv
//
//  Created by Oleksii Horishnii on 2/8/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class TvListVM: RxViewModel {
    struct Input {
        let sortButtonClicked: ControlEvent<Void>
    }
    struct Output {
        let shows: Driver<[TvListCellVM]>
    }

    func transform(input: TvListVM.Input) -> TvListVM.Output {
        fatalError("you need to override this method")
    }
}

class TvListVMImpl: TvListVM {
    private let repo: TvShowRepository
    init(repo: TvShowRepository) {
        self.repo = repo
    }
    
    private let disposeBag = DisposeBag()

    override func transform(input: Input) -> Output {
        let isSorted = input.sortButtonClicked
            .map { true }
            .startWith(false)

        let shows = self.repo.topRated().map { tvShows in
            tvShows.map { TvListCellVM.from(tvShow: $0) }
        }
        
        let sortedShows = Observable.combineLatest(isSorted, shows)
            .map { (arg) -> [TvListCellVM] in
                let (isSorted, shows) = arg
                if !isSorted {
                    return shows
                } else {
                    return shows.sorted { (show1, show2) -> Bool in
                        return show1.name < show2.name
                    }
                }
        }
        
        let driver = sortedShows.asDriver(onErrorJustReturn: [])
        
        return Output(shows: driver)
    }
}
