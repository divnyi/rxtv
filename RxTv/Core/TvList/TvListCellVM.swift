//
//  TvShowCellVM.swift
//  RxTv
//
//  Created by Oleksii Horishnii on 2/8/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import UIKit

struct TvListCellVM: Equatable {
    var name: String
    var imageURL: URL
    
    static func from(tvShow: TvShow) -> TvListCellVM {
        return TvListCellVM(
            name: tvShow.name,
            imageURL: Endpoint.image(path: tvShow.posterPath).url()!
        )
    }
}
