//
//  ViewModel.swift
//  RxTv
//
//  Created by Oleksii Horishnii on 2/8/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import UIKit

protocol RxViewModel {
  associatedtype Input
  associatedtype Output
  
  func transform(input: Input) -> Output
}
