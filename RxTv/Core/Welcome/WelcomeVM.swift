//
//  WelcomeVM.swift
//  RxTv
//
//  Created by Oleksii Horishnii on 2/8/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import UIKit

protocol WelcomeVM {
    func buttonClicked()
}

class WelcomeVMImpl: WelcomeVM {
    private let tvListCoordinator: Coordinator
    
    init(tvListCoordinator: Coordinator) {
        self.tvListCoordinator = tvListCoordinator
    }
    
    func buttonClicked() {
        self.tvListCoordinator.start()
    }
}
