//
//  TvListCoordinator.swift
//  RxTv
//
//  Created by Oleksii Horishnii on 2/8/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import UIKit

class TvListCoordinator: Coordinator {
    // MARK: inputs
    private weak var navigationViewController: UINavigationController?
    
    init(navigationViewController: UINavigationController?) {
        self.navigationViewController = navigationViewController
    }
    
    // MARK: - coordinator
    func start() {
        let resolver = Resolver()

        let vc = resolver.tvListVC()
        
        self.navigationViewController?.pushViewController(vc, animated: true)
    }
}
