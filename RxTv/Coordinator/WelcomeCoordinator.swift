//
//  WelcomeCoordinator.swift
//  RxTv
//
//  Created by Oleksii Horishnii on 2/8/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import UIKit

class WelcomeCoordinator: Coordinator {
    // MARK: inputs
    private weak var navigationViewController: UINavigationController?
    
    init(navigationViewController: UINavigationController?) {
        self.navigationViewController = navigationViewController
    }
    
    // MARK: - coordinator
    func start() {
        let resolver = Resolver()

        resolver.welcomeInput.tvListCoordinator = TvListCoordinator(
            navigationViewController: self.navigationViewController
        )

        let vc = resolver.welcomeScreenVC()
        
        self.navigationViewController?.viewControllers = [vc]
    }
}
