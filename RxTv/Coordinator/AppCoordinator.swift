//
//  AppCoordinator.swift
//  RxTv
//
//  Created by Oleksii Horishnii on 2/8/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import UIKit

class AppCoordinator: Coordinator {
    // MARK: inputs
    private weak var window: UIWindow?
    
    init(window: UIWindow) {
        self.window = window
    }
    
    // MARK: - local variables
    private var navigationController: UINavigationController?
    private var welcomeCoordinator: WelcomeCoordinator?

    // MARK: - coordinator
    func start() {
        self.navigationController = UINavigationController()

        self.welcomeCoordinator = WelcomeCoordinator(
            navigationViewController: self.navigationController
        )

        self.window?.rootViewController = self.navigationController
        self.window?.makeKeyAndVisible()
        
        self.welcomeCoordinator?.start()
    }
}
