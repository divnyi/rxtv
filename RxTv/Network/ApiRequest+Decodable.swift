//
//  ApiRequest+Decodable.swift
//  RxTv
//
//  Created by Oleksii Horishnii on 2/8/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import UIKit
import RxAlamofire
import RxSwift
import Alamofire

extension Decodable {
    static func fetch(endpoint: Endpoint) -> Observable<Self> {
        ApiRequest.fetch(endpoint: endpoint).map { response -> Self in
            let decoder = JSONDecoder()
            let data = response.1
            if let object = try? decoder.decode(Self.self, from: data) {
                return object
            } else {
                throw ApiError.deserializationIssue
            }
        }
    }
}
