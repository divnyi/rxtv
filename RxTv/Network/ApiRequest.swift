//
//  ApiRequest.swift
//  RxTv
//
//  Created by Oleksii Horishnii on 2/8/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import UIKit
import RxAlamofire
import RxSwift
import Alamofire

enum ApiError: Error {
    case malformedUrl
    case serializationIssue
    case deserializationIssue
}

class ApiRequest {
    static func fetch(endpoint: Endpoint) -> Observable<(HTTPURLResponse, Data)> {
        if let url = endpoint.url() {
            var request = URLRequest(url: url)
            do {
                request.httpBody = try endpoint.body()
            } catch {
                return Observable.error(ApiError.serializationIssue)
            }
            request.httpMethod = endpoint.method.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "Accept")

            return RxAlamofire.request(request).responseData()
        } else {
            return Observable.error(ApiError.malformedUrl)
        }
    }
}
