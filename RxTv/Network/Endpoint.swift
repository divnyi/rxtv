//
//  Endpoints.swift
//  RxTv
//
//  Created by Oleksii Horishnii on 2/8/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import UIKit
import Alamofire

enum Endpoint {
    case topRated(page: Int)
    case image(path: String)
    
    var scheme: String { "https" }
    var host: String {
        switch self {
        case .topRated:
            return "api.themoviedb.org"
        case .image:
            return "image.tmdb.org"
        }
    }

    var method: HTTPMethod {
        switch self {
        default:
            return .get
        }
    }

    var path: String {
        switch self {
        case .topRated:
            return "/3/tv/top_rated"
        case .image(let path):
            return "/t/p/w500/\(path)"
        }
    }

    var queryParams: [String: String] {
        switch self {
        case .topRated(let page):
            return [
                "api_key": "25a8f80ba018b52efb64f05140f6b43c",
                "language": "en-US",
                "page": "\(page)",
            ]
        default:
            return [:]
        }
    }

    func body() throws -> Data? {
        switch self {
        default:
            return nil
        }
    }
    
    func url() -> URL? {
        var components = URLComponents()
        components.scheme = self.scheme
        components.host = self.host
        components.path = self.path
        components.queryItems = self.queryParams.map { (arg) -> URLQueryItem in
            let (key, value) = arg
            return URLQueryItem(name: key, value: value)
        }
        return try? components.asURL()
    }
}
