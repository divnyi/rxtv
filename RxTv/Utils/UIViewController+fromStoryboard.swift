//
//  UIViewController+fromStoryboard.swift
//  RxTv
//
//  Created by Oleksii Horishnii on 2/8/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import UIKit

extension UIViewController {
    static func fromStoryboard(storyboardName: String? = nil) -> Self {
        let className = String(describing: Self.self)
        // swiftlint:disable force_cast
        return UIStoryboard(name: storyboardName ?? className, bundle: Bundle.main)
            .instantiateViewController(withIdentifier: className) as! Self
    }
}
