//
//  Constant+Title.swift
//  RxTv
//
//  Created by Oleksii Horishnii on 2/8/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import UIKit

extension Constant {
    class Texts {
        class Welcome {
            static let title = "Welcome"
        }
        class TvList {
            static let title = "Best Tv Shows"
        }
    }
}
