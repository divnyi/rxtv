//
//  Constant+MagicNumbers.swift
//  RxTv
//
//  Created by Oleksii Horishnii on 2/8/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import UIKit

extension Constant {
    class MagicNumbers {
        static let posterHeightToWidth: CGFloat = 735.0/500.0
        static let cellNameplateHeight: CGFloat = 50.0
    }
}
