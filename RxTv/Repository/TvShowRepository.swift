//
//  TvShowRepository.swift
//  RxTv
//
//  Created by Oleksii Horishnii on 2/28/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import RxSwift

protocol TvShowRepository {
    func topRated() -> Observable<[TvShow]>
}

class TvShowRepositoryImpl: TvShowRepository {
    func topRated() -> Observable<[TvShow]> {
        return TopRatedResponse.fetch(endpoint: .topRated(page: 1))
            .map { response -> [TvShow] in
                response.results
        }
    }
}
