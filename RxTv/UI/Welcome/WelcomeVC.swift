//
//  ViewController.swift
//  RxTv
//
//  Created by Oleksii Horishnii on 2/8/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import UIKit

class WelcomeVC: UIViewController {
    // MARK: - incomming variables
    private var vm: WelcomeVM!
    // MARK: - create
    static func createVC(vm: WelcomeVM) -> UIViewController {
        let vc = self.fromStoryboard()
        vc.vm = vm
        return vc
    }
    
    // MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
    }
        
    // MARK: - setup UI
    private func setupUI() {
        self.title = Constant.Texts.Welcome.title
    }
    
    // MARK: - actions
    @IBAction private func buttonPressed(_ sender: Any) {
        self.vm.buttonClicked()
    }
}
