//
//  TvListVC.swift
//  RxTv
//
//  Created by Oleksii Horishnii on 2/8/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import Kingfisher

class TvListVC: UICollectionViewController {
    // MARK: - incomming variables
    private var vm: TvListVM!
    // MARK: - create
    static func createVC(vm: TvListVM) -> UIViewController {
        let vc = self.fromStoryboard()
        vc.vm = vm
        return vc
    }

    // MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
        self.subscribe()
    }
    
    // MARK: - setup UI
    private func setupUI() {
        self.title = Constant.Texts.TvList.title
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Sort",
                                                                 style: .plain,
                                                                 target: nil,
                                                                 action: nil)
    }
    
    // MARK: - subscribe
    private let disposeBag = DisposeBag()
    private func subscribe() {
        let sortButtonClicked = self.navigationItem.rightBarButtonItem!.rx.tap
        
        let input = TvListVM.Input(sortButtonClicked: sortButtonClicked)
        
        let output = self.vm.transform(input: input)

        self.collectionView.delegate = self
        self.collectionView.dataSource = nil

        output.shows
            .drive(self.collectionView.rx.items(
                cellIdentifier: "TvListCell",
                cellType: TvListCell.self
            )) { (_, vm: TvListCellVM, cell: TvListCell) in
                cell.nameLabel.text = vm.name
                cell.imageView.kf.setImage(with: vm.imageURL)
        }
        .disposed(by: self.disposeBag)
    }
}

extension TvListVC: UICollectionViewDelegateFlowLayout {
    // MARK: - 2 per row
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width/2.0
        let height = width * Constant.MagicNumbers.posterHeightToWidth + Constant.MagicNumbers.cellNameplateHeight

        return CGSize(width: width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
