//
//  NetworkApiTests.swift
//  NetworkApiTests
//
//  Created by Oleksii Horishnii on 2/8/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import XCTest
import RxSwift

@testable import RxTv

class NetworkApiTests: XCTestCase {
    let disposeBag = DisposeBag()
    
    // MARK: - top rated
    func testTopRated() {
        let result = TopRatedResponse.fetch(endpoint: .topRated(page: 1))
        let expect = expectation(description: "got results")
        result.subscribe(onNext: { result in
            expect.fulfill()
            print(result)
        }, onError: { error in
            XCTFail("network error \(error)")
            expect.fulfill()
        })
            .disposed(by: self.disposeBag)
        
        waitForExpectations(timeout: 3.0, handler: nil)
    }
}
